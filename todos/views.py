from django.shortcuts import render
from .models import TodoList
from django.http import HttpRequest

def my_list(request: HttpRequest):
    todolists = TodoList.objects.all()
    context = {
        'todo_lists_object': todolists
    }
    return render(request, 'todos/list.html', context)
